package com.doapps.petservices.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.doapps.petservices.Adapters.PostAdapter;
import com.doapps.petservices.Models.Post;
import com.doapps.petservices.Network.Models.PostResponse;
import com.doapps.petservices.PetServicesApplication;
import com.doapps.petservices.R;
import com.doapps.petservices.Utils.PreferenceManager;
import com.doapps.petservices.Utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Percy on 07/05/2017.
 */

public class PublicacionesFragment extends Fragment {

    RecyclerView rv_post;
    PostAdapter adapter;
    ProgressBar pb;
    Button bt_fitlrar;
    Spinner sp_filter;
    TextView tv_expand;
    LinearLayout ll_container;

    boolean filterVisible = false;

    private String[] types;

    PreferenceManager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_publicaciones,container,false);

        manager = PreferenceManager.getInstance(getContext());

        getType();

        setupViews(v);

        getAllPost();

        return v;
    }

    private void getType(){
        types = getResources().getStringArray(R.array.sp_all_types);
    }

    private void setupViews(View v){
        rv_post = (RecyclerView) v.findViewById(R.id.rv_post);
        pb = (ProgressBar) v.findViewById(R.id.pb);
        bt_fitlrar = (Button) v.findViewById(R.id.bt_fitlrar);
        sp_filter = (Spinner) v.findViewById(R.id.sp_filter);
        tv_expand = (TextView) v.findViewById(R.id.tv_expand);
        ll_container = (LinearLayout) v.findViewById(R.id.ll_container);

        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, types);
        sp_filter.setAdapter(adapter);

        tv_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        filterVisible ? 0 : LinearLayout.LayoutParams.WRAP_CONTENT));
                tv_expand.setCompoundDrawablesWithIntrinsicBounds(null,null,
                        filterVisible ? ContextCompat.getDrawable(getActivity(),android.R.drawable.arrow_down_float) :  ContextCompat.getDrawable(getActivity(),android.R.drawable.arrow_up_float),null);
                filterVisible = !filterVisible;
            }
        });



        bt_fitlrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Map<String, String> map = new HashMap<>();
                    map.put("Type", sp_filter.getSelectedItem().toString());

                    if (!sp_filter.getSelectedItem().toString().equals(types[0])) {
                        pb.setVisibility(View.VISIBLE);
                        rv_post.setVisibility(View.GONE);

                        Call<ArrayList<PostResponse>> call = PetServicesApplication.getInstance().getServices().filterPost(map);

                        call.enqueue(new Callback<ArrayList<PostResponse>>() {
                            @Override
                            public void onResponse(Call<ArrayList<PostResponse>> call, Response<ArrayList<PostResponse>> response) {
                                if (response.isSuccessful()) {
                                    setupRv(response.body());
                                    ll_container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                                    tv_expand.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), android.R.drawable.arrow_down_float), null);
                                    filterVisible = !filterVisible;
                                }
                                pb.setVisibility(View.GONE);
                                rv_post.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onFailure(Call<ArrayList<PostResponse>> call, Throwable t) {
                                pb.setVisibility(View.GONE);
                                rv_post.setVisibility(View.VISIBLE);
                                Utils.showToastInternalServerError(getActivity());
                            }
                        });
                    } else {
                        getAllPost();
                    }

            }
        });
    }

    private void getAllPost() {
        pb.setVisibility(View.VISIBLE);
        rv_post.setVisibility(View.GONE);

        Call<ArrayList<PostResponse>> call = PetServicesApplication.getInstance().getServices().getAllPost();

        call.enqueue(new Callback<ArrayList<PostResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<PostResponse>> call, Response<ArrayList<PostResponse>> response) {
                if(response.isSuccessful()){
                    setupRv(response.body());
                }
                pb.setVisibility(View.GONE);
                rv_post.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<ArrayList<PostResponse>> call, Throwable t) {
                pb.setVisibility(View.GONE);
                rv_post.setVisibility(View.VISIBLE);
                Utils.showToastInternalServerError(getActivity());
            }
        });
    }

    private void setupRv(ArrayList<PostResponse> body){
        adapter = new PostAdapter(getActivity(),body,null,true);
        rv_post.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_post.setAdapter(adapter);
    }
}