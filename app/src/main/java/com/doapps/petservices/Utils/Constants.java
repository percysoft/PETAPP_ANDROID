package com.doapps.petservices.Utils;

/**
 * Created by Percy on 11/06/2017.
 */

public class Constants {

    public static String ROLE_USER =    "595b06ed84af7d6a5268df8f";
    public static String ROLE_COMPANY = "595b06ed84af7d6a5268df8e";

    public static int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public static final String SIGN_UP_EMPRESA = "sign_up_empresa";
    public static final String SIGN_UP_PERSONA = "sign_up_persona";
    public static final int CAMERA_REQUEST = 1;
    public static final int REQUEST_IMAGE_GALLERY = 2;
    public static final int CREATE_POST_REQUEST = 3;
    public static final int UPDATE_USER_REQUEST = 4;
    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_PHOTO = "extra_photo";
}
