package com.doapps.petservices.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doapps.petservices.Adapters.PostAdapter;
import com.doapps.petservices.Network.Models.PostResponse;
import com.doapps.petservices.Network.Models.UserData;
import com.doapps.petservices.PetServicesApplication;
import com.doapps.petservices.R;
import com.doapps.petservices.Utils.Constants;
import com.doapps.petservices.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

public class UserProfile extends AppCompatActivity {

    LinearLayout ll_container;
    TextView tv_nombre;
    ImageView iv_photo;
    TextView tv_phone;
    TextView tv_email;
    RecyclerView rv_post;
    ProgressBar pb;

    private PostAdapter adapter;

    private String extraId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        getExtras();

        setupViews();

        getUserData();
    }

    private void getExtras() {
        if(getIntent().hasExtra(Constants.EXTRA_ID)){
            extraId = getIntent().getStringExtra(Constants.EXTRA_ID);
        }
    }

    private void getUserData() {
        pb.setVisibility(View.VISIBLE);
        ll_container.setVisibility(View.GONE);
        Call<UserData> call = PetServicesApplication.getInstance().getServices().getUserData(extraId);

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                if(response.isSuccessful()){

                    if(response.body().getName() != null && !response.body().getName().isEmpty()){
                        tv_nombre.setText(response.body().getName());
                    }else{
                        tv_nombre.setText(response.body().getOrganization());
                    }

                    if(response.body().getFbId() != null){
                        String newUrl = response.body().getPicture().substring(0, 4) + "s" + response.body().getPicture().substring(4, response.body().getPicture().length());
                        Picasso.with(UserProfile.this).load(newUrl).into(iv_photo);
                    }else{
                        Picasso.with(UserProfile.this).load(response.body().getImage().getUrl()).into(iv_photo);
                    }

                    tv_phone.setText(response.body().getPhone());

                    tv_email.setText(response.body().getEmail());

                    getUserPost();
                }
                pb.setVisibility(View.GONE);
                ll_container.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                pb.setVisibility(View.GONE);
                ll_container.setVisibility(View.VISIBLE);
                Utils.showToastInternalServerError(UserProfile.this);
            }
        });
    }

    private void getUserPost() {
        pb.setVisibility(View.VISIBLE);
        rv_post.setVisibility(View.GONE);
        Call<ArrayList<PostResponse>> call = PetServicesApplication.getInstance().getServices().getUserPosts(extraId);

        call.enqueue(new Callback<ArrayList<PostResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<PostResponse>> call, Response<ArrayList<PostResponse>> response) {
                if(response.isSuccessful()){
                    setupRv(response.body());
                }
                pb.setVisibility(View.GONE);
                rv_post.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<ArrayList<PostResponse>> call, Throwable t) {
                Utils.showToastInternalServerError(UserProfile.this);
                pb.setVisibility(View.GONE);
                rv_post.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupRv(ArrayList<PostResponse> body) {
        adapter = new PostAdapter(this, body, rv_post,false);
        rv_post.setLayoutManager(new LinearLayoutManager(this));
        rv_post.setAdapter(adapter);
    }

    private void setupViews() {
        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        tv_nombre = (TextView) findViewById(R.id.tv_nombre);
        iv_photo = (ImageView) findViewById(R.id.iv_photo);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_email = (TextView) findViewById(R.id.tv_email);
        rv_post = (RecyclerView) findViewById(R.id.rv_post);
        pb = (ProgressBar) findViewById(R.id.pb);
    }
}
